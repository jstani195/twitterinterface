﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using LinqToTwitter;
using TwitterInterface.Helpers;
using TwitterInterface.Models.Home;

namespace TwitterInterface.Controllers
{
    public class HomeController : Controller
    {
        private readonly TwitterContext _context;

        public HomeController()
        {
            //If this was more than a single page we may consider treating this like a real context and wrapping it/injecting it with an IOC container
            var auth = new MvcAuthorizer
            {
                CredentialStore = new SessionStateCredentialStore()
                {
                    ConsumerKey = ConfigurationManager.AppSettings["ConsumerKey"],
                    ConsumerSecret = ConfigurationManager.AppSettings["ConsumerSecret"],
                    OAuthToken = ConfigurationManager.AppSettings["OAuthToken"],
                    OAuthTokenSecret = ConfigurationManager.AppSettings["OAuthTokenSecret"]
                }
            };

            _context = new TwitterContext(auth);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Search(string searchText, ulong? sinceID, ulong? maxID)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                return PartialView("TwitterSearchResponsePartial", null);
            }

            var searchResults = GetSearchResults(searchText, sinceID, maxID);

            if (searchResults == null)
            {
                return PartialView("TwitterSearchResponsePartial", null);
            }

            return PartialView("TwitterSearchResponsePartial", new TwitterSearchResponse(searchResults.Statuses));
        }

        public async Task<ActionResult> FavoriteTweet(ulong statusID)
        {
            try
            {
                var status = await _context.CreateFavoriteAsync(statusID);

                if(status == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                return Json(new { statusID }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        private Search GetSearchResults(string searchText, ulong? sinceID, ulong? maxID)
        {
            //If this was more than a single page and actually going to be used, we may consider wrapping this elsewhere
            return (from search in _context.Search
                    where search.Type == SearchType.Search &&
                            search.ResultType == ResultType.Popular &&
                            search.Query == searchText.AddHashtag() &&
                            search.Count == 4 &&
                            search.SinceID == (sinceID ?? 0) &&
                            search.MaxID == (maxID ?? 0)
                    select search).SingleOrDefault();
        }
    }
}
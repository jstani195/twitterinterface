﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TwitterInterface.Startup))]
namespace TwitterInterface
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

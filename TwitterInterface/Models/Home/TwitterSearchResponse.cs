﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LinqToTwitter;

namespace TwitterInterface.Models.Home
{
    public class TwitterSearchResponse
    {
        public TwitterSearchResponse(List<Status> statuses)
        {
            Statuses = statuses;

            if (statuses.Any())
            {
                MaxID = statuses.Max(i => i.StatusID);
                MinID = statuses.Min(i => i.StatusID) - TwitterInclusionNegator;
            }
        }

        public ulong MinID { get; set; }
        public ulong MaxID { get; set; }
        public List<Status> Statuses { get; set; }
        private const ulong TwitterInclusionNegator = 1;
    }
}
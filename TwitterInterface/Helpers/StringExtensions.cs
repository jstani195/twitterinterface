﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TwitterInterface.Helpers
{
    public static class StringExtensions
    {
        public static string AddHashtag(this string str)
        {
            if (string.IsNullOrEmpty(str) || str.StartsWith("#"))
                return str;

            return string.Format("#{0}", str);
        }
    }
}